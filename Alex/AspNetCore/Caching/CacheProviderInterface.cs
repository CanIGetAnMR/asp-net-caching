using System;
using System.Transactions;
using Microsoft.AspNetCore.Http;

namespace Alex.AspNetCore.Caching
{
    public abstract class CacheProviderInterface
    {
        internal static string NeedsCacheKey => "needs-cache";
        
        //Call this in your controller response handler to cache your response
        public static void CacheResponse(HttpContext context, ICacheInputItem inputItem)
        {
            if (!context.Items.ContainsKey(NeedsCacheKey))
            {
                throw new CachingException("Controller response handler not tagged '[CachedResponse]'");
            }

            AbstractResponseCacheProvider cacheProvider = (AbstractResponseCacheProvider) context.Items[NeedsCacheKey];
            string key = cacheProvider.GetCacheKey(context);
            cacheProvider.CacheItem(key, inputItem);
        }

        private class CachingException : Exception
        {
            internal CachingException(string message) : base(message)
            {
                
            }
        }
    }
}